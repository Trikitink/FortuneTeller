package FortuneUi;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.*;

public class MainGui extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JComboBox<String> signType;
	private JComboBox<String> timePeriod;
	private JButton getFortune = new JButton("Generuj Horoskop");
	private JPanel panel = new JPanel();
	
	public MainGui(String[] times, String[] signs){
		
		signType = new JComboBox<String>(signs);
		timePeriod = new JComboBox<String>(times);
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(250,100);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.setResizable(false);
		
		panel.add(timePeriod);
		panel.add(signType);
		panel.add(getFortune);
		
		this.add(panel);
	}
	
	public String getSign(){
		
		return signType.getSelectedItem().toString();
		
	}
	
	public String getPeriod(){
		
		return timePeriod.getSelectedItem().toString();
		
	}
	
	public void addListenerToButton(ActionListener e){
		
		getFortune.addActionListener(e);
		
	}
	
	public void disableButton(){
		
		getFortune.setEnabled(false);
		
	}
	
	public void enableButton(){
		
		getFortune.setEnabled(true);
		
	}
	
	public HoroscopeWindow showFortune(Vector<String> write){

		return new HoroscopeWindow(write);
		
	}
	
	
}

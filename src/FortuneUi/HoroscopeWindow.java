package FortuneUi;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.*;


public class HoroscopeWindow extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		private JButton ok = new JButton("OK");
		private JPanel okPanel = new JPanel();
		private JTextArea info = new JTextArea(20,10);
		private JScrollPane scroll;
		
	
	public HoroscopeWindow(Vector<String> infoToWrite) {
		
		this.setSize(600,600);
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		
		info.setWrapStyleWord(true);
		info.setLineWrap(true);
		info.setFont(new Font("Verdana", Font.BOLD, 15));
		info.setEditable(false);
		info.setBackground(null);
		info.setMargin(new Insets(10, 10, 10, 10));


		scroll = new JScrollPane(info);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        
		for (String write : infoToWrite){
			
			info.append(write + "\n\n");
			
		}
		
		info.setCaretPosition(0);
		
		ok.setAlignmentX(CENTER_ALIGNMENT);
		
		okPanel.setLayout(new BoxLayout(okPanel,BoxLayout.Y_AXIS));
		okPanel.add(scroll);
		okPanel.add(ok);
		
		this.add(okPanel);
		
	}
	
	public void closeWindow(){
		
		this.dispose();
		
	}
	
	public void addClosingAction(ActionListener e){
		
		ok.addActionListener(e);
		
	}
	
}

package DataExtractors;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

public abstract class DataExtractor {
	
	public Vector<String> getFileStream(String sign, String period){
		
		String filePath = sign + "_" + period + ".html";
		//System.out.println(sign);
		File dataFile = new File("./tmp/" + filePath);
		
		Vector<String> dataStrings = new Vector<String>(0);
		
		try {
			
			BufferedReader dataStream = new BufferedReader (new FileReader(dataFile));
			
			String dataTemporaryString = dataStream.readLine();
			
			while (dataTemporaryString != null){
				
				dataStrings.addElement(dataTemporaryString);

				dataTemporaryString = dataStream.readLine();
				
				System.out.println(dataTemporaryString);
			}
			
			dataStream.close();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		}
		
		return dataStrings;
	}
	
	public Vector<String> dataCleanUp(Vector<String> dataStrings){
		
		Vector<String> cleanedData = new Vector<String>(0);
		
		for (String data : dataStrings){
				 
				cleanedData.add(data.substring(3, data.length()-4));		
			
		}
		
		int numOfIterations = cleanedData.size();
		System.err.println(numOfIterations);
		for(int i = cleanedData.size()-1 ; i >= numOfIterations - 2 ; --i){
			
			System.err.println("LOLOLOLOLOLOLOLO");

			cleanedData.remove(i);
			
		}
		
		return cleanedData;
		
	}
	
}

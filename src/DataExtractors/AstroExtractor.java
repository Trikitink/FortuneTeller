package DataExtractors;

import java.util.Vector;

public class AstroExtractor extends DataExtractor{
	
	public Vector<String> Data;
	
	public AstroExtractor(String sign, String period) {

		Data = dataCleanUp(getFileStream(sign, period));
		
	}
	
	public Vector<String> getDataToShow(){
		
		return Data;
		
	}
	
	public Vector<String> dataCleanUp(Vector<String> dataStrings){
		
		Vector<String> cleanedData = new Vector<String>(0);
		
		for (String data : dataStrings){
			
				data = astroReplace(data);
				 
				cleanedData.add(data.substring(3, data.length()-4));		
			
		}
		
		int numOfIterations = cleanedData.size();
		System.err.println(numOfIterations);
		for(int i = cleanedData.size()-1 ; i >= numOfIterations - 2 ; --i){
			
			System.err.println("LOLOLOLOLOLOLOLO");

			cleanedData.remove(i);
			
		}
		
		return cleanedData;
		
	}
	
	public String astroReplace(String data){
		
		data = data.replace("&#8211;", " - ");
		
		data = data.replace("&nbsp;", " ");
		
		data = data.replace("&#8222;", "\"");
		
		data = data.replace("&#8221", "\"");
		
		data = data.replace("&#8230;", "...");
		
		return data;
		
	}

}

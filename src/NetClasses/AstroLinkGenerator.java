package NetClasses;

public final class AstroLinkGenerator {
	
	public static final String WEEKLY = "tygodniowy"; 
	public static final String MONTHLY = "miesieczny"; 
	public static final String YEARLY = "roczny"; 
	
	public static final String PISCES = "ryby"; 
	public static final String AQUARIUS = "wodnik"; 
	public static final String SCORPIO = "skorpion"; 
	public static final String ARIES = "baran";
	public static final String TAURUS = "byk";
	public static final String GEMINI = "bliznieta";
	public static final String CANCER = "rak";
	public static final String LEO = "lew";
	public static final String VIRGO = "panna";
	public static final String LIBRA = "waga";
	public static final String SAGITTARIUS = "strzelec";
	public static final String CAPRICORN = "koziorozec";
	
	private static String linkBase = "http://astro.pl/";
	
	public static String generateHoroscopeLink(String aquarius, String weekly){
		
		String fullLink;
		
		fullLink = linkBase + aquarius + "-horoskop-" + weekly + "/";
		
		return fullLink;
	}

	public static String[] getAllSigns(){
		
		String[] allSigns = {PISCES, AQUARIUS, SCORPIO,
							ARIES, TAURUS, GEMINI,
							CANCER, LEO, VIRGO,
							LIBRA, SAGITTARIUS, CAPRICORN};
		
		return allSigns;
		
	}
	
	public static String[] getAllTimePeriods(){
		
		String[] allPeriods = {WEEKLY, MONTHLY, YEARLY};
		
		return allPeriods;
		
	}
	
}

package NetClasses;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;

public class HtmlSaver {
	
	private URL url;
	private URLConnection connection;
	
	public void saveHtmlToFile(String sign, String timePeriod){
		
		try {
			
			url = new URL(AstroLinkGenerator.generateHoroscopeLink(sign, timePeriod));
			
			connection = url.openConnection();
			
			BufferedReader inConnect = new BufferedReader(
										new InputStreamReader(
											connection.getInputStream(), "UTF-8"));
			
			String oneCodeLine;
			
			PrintWriter writeToFile = createTemporaryFile(sign + "_" + timePeriod);
			
			while ((oneCodeLine = inConnect.readLine()) != null){
				
				if ((oneCodeLine.startsWith("<p>") && (oneCodeLine.endsWith("</p>"))))
				writeToFile.println(oneCodeLine);
				
			}
			System.err.println("Pobieranie " + sign + " " + timePeriod + " zakonczone sukcesem");
			writeToFile.close();
			
		} catch (MalformedURLException e) {
			
			e.printStackTrace();
			
		} catch (IOException e){
			
			e.printStackTrace();
			
		}
		
		
		
	}
	
	public PrintWriter createTemporaryFile(String sign){
		
		File tempDirectory = new File("./tmp");
		tempDirectory.mkdirs();
		
		try {
			File tempFile = new File("./tmp/" + sign + ".html" );
			
			tempFile.createNewFile();
			
			PrintWriter saver = new PrintWriter (
									new BufferedWriter(
											(new FileWriter (tempFile))));
			return saver;
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		}
		
		return null;
		
	}

}

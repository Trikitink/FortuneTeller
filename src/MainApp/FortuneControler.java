package MainApp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import DataExtractors.AstroExtractor;
import FortuneUi.*;
import NetClasses.*;

public class FortuneControler {
	
	MainGui mainView;
	HtmlSaver saver;
	HoroscopeWindow fortune;
	
	FortuneControler(MainGui mainViewX, HtmlSaver saverX){
		
		mainView = mainViewX;
		
		saver = saverX;
		
		mainView.addListenerToButton(new GenerateHoroscope());
		
	}
	
	private class GenerateHoroscope implements ActionListener{
		
	
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			mainView.disableButton();
			
			saver.saveHtmlToFile(mainView.getSign(), mainView.getPeriod());
			AstroExtractor x = new AstroExtractor(mainView.getSign(), mainView.getPeriod());
			
			for (String data : x.Data){
				
				System.err.println(data);
				
			}
			
			fortune = mainView.showFortune(x.getDataToShow());
			
			fortune.addClosingAction(new CloseWindowAction());
			
		}
		
		
	}
	
	private class CloseWindowAction implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent X) {

			fortune.dispose();
			
			mainView.enableButton();
			
		}
		
	}
	
}

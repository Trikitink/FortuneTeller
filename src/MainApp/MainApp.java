package MainApp;

import NetClasses.*;
import FortuneUi.MainGui;

public class MainApp {

	public static void main(String[] args) {
		
		HtmlSaver saver = new HtmlSaver();

		MainGui mainView = new MainGui(AstroLinkGenerator.getAllTimePeriods(),
				AstroLinkGenerator.getAllSigns());
		
		new FortuneControler(mainView, saver);
		
	}
}
